﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PopupScript : MonoBehaviour
{
    public Button CloseBtn;
    public GameObject[] Animtebj;
    public bool FrameSlide = false;
    public VideoPlayer videoPlayer;
    public VideoClip videoClipEn;
    public VideoClip videoClipAr;

    bool SlideAnimisRun = false;
    bool SlidesIsShowing = false;


    private void Awake()
    {
        SettingsManager.instance.toDoEvent.AddListener(() => SetVideoLanguage());
    }
    // Start is called before the first frame update
    void Start()
    {
        CloseBtn.onClick.AddListener(() => HidePopup());
        videoPlayer.prepareCompleted += VideoPlayer_PrepareCompleted;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void VideoPlayer_PrepareCompleted(VideoPlayer source)
    {
        SettingsManager.instance.BackgroundVideo.Pause();
        source.time = 0;
        source.Play();
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "time", .5f, "easetype", iTween.EaseType.easeOutBack));
        Loom.QueueOnMainThread(() => {
            Showbjects();
        }, 0.5f);
    }


    // SHOW/HIDE POPUP
    public void ShowPopup()
    {
        HideObj();
        gameObject.SetActive(true);
        SlidesIsShowing = false;
        if (!videoPlayer)
        {
            iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "time", .5f, "easetype", iTween.EaseType.easeOutBack));
            Loom.QueueOnMainThread(() => {
                Showbjects();
            }, 0.5f);
        }
        else
        {
            SetVideoLanguage();
            videoPlayer.Prepare();
        }


    }

    public void HidePopup() {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", .5f, "easetype", iTween.EaseType.easeInBack));
        Loom.QueueOnMainThread(() => {
            gameObject.SetActive(false);
        }, .6f);
        if (videoPlayer)
        {
            SettingsManager.instance.BackgroundVideo.Play();
            videoPlayer.Stop();
        }
    }

    public void Showbjects()
    {
        if (!FrameSlide)
        {
            float delay = 0f;
            foreach (GameObject obj in Animtebj)
            {
                iTween.ScaleTo(obj, iTween.Hash("scale", Vector3.one, "time", .5f, "delay", delay, "easetype", iTween.EaseType.easeOutBack));
                delay += 0.1f;
            }
        }
    }
    void HideObj() {
        foreach (GameObject obj in Animtebj) {
            obj.transform.localScale = Vector3.zero;
        }
    }


    public void ShowSlides()
    {
        if (!SlideAnimisRun)
        {
            if (!SlidesIsShowing)
            {
                SlidesIsShowing = true;
                StartCoroutine(ShowInformFrame());
            }
            else
            {
                SlidesIsShowing = false;
                StartCoroutine(HideInformFrame());
            }
        }
    }

    IEnumerator ShowInformFrame() {
        SlideAnimisRun = true;
        for (int i = 0; i < Animtebj.Length; i++)
        {
            GameObject obj = Animtebj[i];
            obj.GetComponent<Animator>().SetFloat("Direction", 1);
            obj.GetComponent<Animator>().Play("InformationFrameAnim", -1, 0);
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(1f);
        SlideAnimisRun = false;
    }

    IEnumerator HideInformFrame()
    {
        SlideAnimisRun = true;
        for (int i = Animtebj.Length - 1; i >= 0; i--)
        {
            GameObject obj = Animtebj[i];
            obj.GetComponent<Animator>().SetFloat("Direction", -1);
            obj.GetComponent<Animator>().Play("InformationFrameAnim", -1, 1);
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(1f);
        SlideAnimisRun = false;
    }

    void SetVideoLanguage()
    {
        if (videoPlayer)
        {
            if (SettingsManager.instance.LanguageIndex == 0)
            {
                videoPlayer.clip = videoClipEn;
            }
            else
            {
                videoPlayer.clip = videoClipAr;
            }
        }
    }
}
