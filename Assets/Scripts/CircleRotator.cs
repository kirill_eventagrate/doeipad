﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircleRotator : MonoBehaviour
{
    public RectTransform[] Circles;
    public float[] speeds;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Circles.Length == speeds.Length)
        {
            for (int i = 0; i < Circles.Length; i++) {
                Circles[i].Rotate(new Vector3(0, 0, speeds[i]));
            }
        }
    }
}
