﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageLanguageSwitcher : MonoBehaviour
{
    public GameObject EngTextImage;
    public GameObject ArabTextImage;

    private void Awake()
    {
        SettingsManager.instance.toDoEvent.AddListener(() => SetTextImageLanguage());
        SetTextImageLanguage();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetTextImageLanguage()
    {
        if (SettingsManager.instance.LanguageIndex == 0)
        {
            EngTextImage.SetActive(true);
            ArabTextImage.SetActive(false);
        }
        else
        {
            EngTextImage.SetActive(false);
            ArabTextImage.SetActive(true);
        }
    }
}
