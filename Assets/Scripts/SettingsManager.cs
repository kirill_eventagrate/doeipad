﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Events;

public class SettingsManager : MonoBehaviour
{
    static public SettingsManager instance = null;

    public Entity_English excelTexts;
    public int LanguageIndex = 0;
    public VideoPlayer BackgroundVideo;
    public Button[] LanguageBtns;

    public UnityEvent toDoEvent;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        excelTexts = ExcelImportRunTime.ReadExcel(excelTexts, Application.persistentDataPath + "/DOE_Immersive_Room.xlsx", 4);
    }
    // Start is called before the first frame update
    void Start()
    {
        /*string[] texts = excelTexts.GetDataById(3, 15).Arab.Split('|');
        foreach (string str in texts)
        {
            Debug.Log(str);
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLanguageIndex(int _languageIndex) { 
        LanguageIndex = _languageIndex;
        foreach (Button btn in LanguageBtns)
        {
            btn.interactable = true;
        }
        LanguageBtns[LanguageIndex].interactable = false;
        toDoEvent.Invoke();
    }
}