﻿using System.Collections;
using System.Collections.Generic;
using RTLTMPro;
using UnityEngine;
using UnityEngine.UI;

public class LanguageTextes : MonoBehaviour
{
    public Text EngText;
    public RTLTextMeshPro ArabText;
    public int SheetId;
    public int RowId;

    Entity_English.ProjectParam Textes;

    private void Awake()
    {
        SettingsManager.instance.toDoEvent.AddListener(() => SetLanguage());
    }
    // Start is called before the first frame update
    void Start()
    {
        Textes = SettingsManager.instance.excelTexts.GetDataById(SheetId, RowId);
        string FinalEngText = Textes.English.Replace("<RED>", "<color=#C40001ff>");
        FinalEngText = FinalEngText.Replace("</RED>", "</color>");
        EngText.text = FinalEngText;

        string FinalArabText = Textes.Arab.Replace("<RED>", "<color=#C40001ff>");
        FinalArabText = FinalArabText.Replace("</RED>", "</color>");
        ArabText.text = FinalArabText;
        SetLanguage();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetLanguage()
    {
        if (SettingsManager.instance.LanguageIndex == 0)
        {
            EngText.gameObject.SetActive(true);
            ArabText.gameObject.SetActive(false);
        }
        else
        {
            EngText.gameObject.SetActive(false);
            ArabText.gameObject.SetActive(true);
        }
    }
}
