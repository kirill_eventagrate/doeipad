﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject StartScreen;
    public GameObject MainScreen;
    public GameObject EnergyCubeScreen;
    public GameObject IntegratedPFScreen;

    public Animator StartScreenAnimator;
    public Animator MainMenuAnimator;
    public Animator EnergyCubeAnimator;
    public Animator IntegratedPFAnimator;

    public ScreenScript[] Screens;
    public int ScreenIndex = 0;
    public Button StartBtn;

    bool playAnim = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SwithingScreens(int _ScreenIndex)
    {
        if (!playAnim) {
            playAnim = true;
            Screens[ScreenIndex].HideScreen();
            Screens[_ScreenIndex].ShowScreen();
            ScreenIndex = _ScreenIndex;
            Loom.QueueOnMainThread(() => { playAnim = false; }, 1f);
        }

    }


    public void ShowPopup(PopupScript _popup) {
        _popup.ShowPopup();
    }
}
