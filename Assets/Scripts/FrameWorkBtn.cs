﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameWorkBtn : MonoBehaviour
{
    public float RandomRadius;
    public float speed = 5;
    public Vector2 StartLocation;
    public Vector2 Target;

    public float LerpVal;

    Animator animator;

    Random rand = new Random();
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        StartLocation = gameObject.GetComponent<RectTransform>().localPosition;
        Target = StartLocation;
        //InvokeRepeating("UpdateTarget", Random.Range(0.1f, 0.4f), 1f);
        //Invoke("StartAnimation", Random.Range(0.1f, 2f));
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void LateUpdate()
    {
        //gameObject.GetComponent<RectTransform>().localPosition = Vector2.MoveTowards(gameObject.GetComponent<RectTransform>().localPosition, Target, Time.deltaTime * speed);
        //gameObject.GetComponent<RectTransform>().localPosition = Vector2.Lerp(gameObject.GetComponent<RectTransform>().localPosition, Target, Time.deltaTime * speed);
        gameObject.GetComponent<RectTransform>().localScale = Vector3.Lerp(Vector3.one, new Vector3(1.035f, 1.035f, 1.035f), LerpVal);
    }

    void UpdateTarget()
    {
        Target = StartLocation + Random.insideUnitCircle * RandomRadius;
    }

    void StartAnimation() {
        animator.Play("BtnPulse", -1, 0);
    }

    private void OnEnable()
    {
        Invoke("StartAnimation", Random.Range(0.1f, 2f));
    }
}
