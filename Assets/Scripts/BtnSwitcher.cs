﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnSwitcher : MonoBehaviour
{
    Animator animator;
    bool isAnimPlay = false;
    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void NextBtn()
    {
        if (!isAnimPlay)
        {
            animator.SetFloat("Direction", 1f);
            animator.Play("SwichingTwoBigBtns", -1, 0);
            isAnimPlay = true;
            Loom.QueueOnMainThread(() => { isAnimPlay = false; }, 0.8f);
        }

    }

    public void PrevBtn() {
        if (!isAnimPlay) {
            animator.SetFloat("Direction", -1f);
            animator.Play("SwichingTwoBigBtns", -1, 1);
            isAnimPlay = true;
            Loom.QueueOnMainThread(() => { isAnimPlay = false; }, 0.8f);
        }
    }
}
