﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenScript : MonoBehaviour
{
    public Animator animator;
    public string AnimationName;
    public float StartAnimDirection;
    public int StartAnimTime;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowScreen() {
        gameObject.SetActive(true);
        animator.SetFloat("Direction", StartAnimDirection);
        animator.Play(AnimationName, -1, StartAnimTime);
    }

    public void HideScreen() {
        animator.SetFloat("Direction", -StartAnimDirection);
        animator.Play(AnimationName, -1, 1 - StartAnimTime);

        Loom.QueueOnMainThread(() => {
            gameObject.SetActive(false);
        }, 1f);
    }
}
