﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

public class ExcelImportRunTime 
{
    static public Entity_English ReadExcel(Entity_English _exelData, string _fileName, int _sheetCount)
    {
        _exelData.sheets.Clear();
        string filePath = _fileName;
        using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            IWorkbook book = null;
            if (Path.GetExtension(filePath) == ".xls")
            {
                book = new HSSFWorkbook(stream);
            }
            else
            {
                book = new XSSFWorkbook(stream);
            }
            //book.GetSheet
            //stream.Position = 0;
            for (int j = 0; j < _sheetCount; j++)
            {
                string sheetName = book.GetSheetName(j);
                ISheet sheet = book.GetSheet(sheetName);
                if (sheet == null)
                {
                    Debug.LogError("[QuestData] sheet not found:" + sheetName);
                    return null;
                }

                Entity_English.Sheet s = new Entity_English.Sheet();
                s.name = sheetName;

                for (int i = 2; i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    ICell cell = null;

                    Entity_English.ProjectParam p = new Entity_English.ProjectParam();

                    //cell = row.GetCell(0); p.Text_id = (cell == null ? "" : cell.NumericCellValue.ToString());
                    cell = row.GetCell(2); p.English = (cell == null ? "" : cell.StringCellValue);
                    cell = row.GetCell(3); p.Arab = (cell == null ? "" : cell.StringCellValue);
                    s.TextValueslist.Add(p);
                }
                _exelData.sheets.Add(s);
            }
            stream.Close();
        }

        return _exelData;
    }


    static public void AddExcelRow(List<object[]> _rowData, string _filePath, string _sheetName)
    {
        using (FileStream stream = File.Open(_filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            stream.Position = 0;
            IWorkbook book = null;
            if (Path.GetExtension(_filePath) == ".xls")
            {
                book = new HSSFWorkbook(stream);
            }
            else
            {
                book = new XSSFWorkbook(stream);
            }
            string sheetName = _sheetName;
            ISheet sheet = book.GetSheet(sheetName);
            if (sheet == null)
            {
                Debug.LogError("[QuestData] sheet not found:" + sheetName);
            }
            else
            {
                int StartRowPos = sheet.LastRowNum + 1;
                for (int j = 0; j < _rowData.Count; j++)
                {
                    IRow newRow = sheet.CreateRow(StartRowPos + j);
                    Debug.Log("LastRowNum: " + sheet.LastRowNum);
                    for (int i = 0; i < _rowData[j].Length; i++)
                    {
                        ICell cell = newRow.CreateCell(i);
                        cell.SetCellValue(_rowData[j][i].ToString());
                        Debug.Log(_rowData[j][i].ToString());
                    }
                }
            }
            stream.Close();
            using (FileStream writeStream = new FileStream(_filePath, FileMode.Create, FileAccess.Write))
            {
                book.Write(writeStream);
                writeStream.Close();
            }
            
        }
    }
}
